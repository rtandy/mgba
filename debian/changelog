mgba (0.10.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - Drop GCC 14 patch, applied upstream.
  * Update Standards-Version to 4.7.0. No changes needed.
  * Fix/improve AppStream metadata files:
    - debian/io.mgba.libretro-mgba.metainfo.xml:
      + Change the component ID to rDNS format. Fixes an appstreamcli
        validation warning (cid-is-not-rdns).
      + Rename libretro-mgba.metainfo.xml to follow the cID change.
      + Update retroarch's component ID in <extends>.
    - debian/io.mgba.mGBA.appdata.xml:
      + Rename mgba-qt.appdata.xml to match the component ID. Fixes an
        appstreamcli validation warning (metainfo-filename-cid-mismatch).
      + Add launchable tag. Fixes an appstreamcli validation info
        (desktop-app-launchable-omitted).

 -- Ryan Tandy <ryan@nardis.ca>  Mon, 23 Dec 2024 17:10:53 -0800

mgba (0.10.3+dfsg-2) unstable; urgency=medium

  * Import upstream patch to fix FTBFS with GCC 14. (Closes: #1075267)

 -- Ryan Tandy <ryan@nardis.ca>  Sat, 27 Jul 2024 11:48:18 -0700

mgba (0.10.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - added support for FFmpeg 7.0 (Closes: #1072434)
  * Exclude headers related to foreign threading platforms, which cannot be
    included on Debian, from libmgba-dev. See #1062872.
  * debian/gitlab-ci.yml: Update to salsa-ci pipeline.
  * debian/watch: Update to version 4 and use GitHub releases API.
  * Update Standards-Version to 4.6.2. No changes needed.
  * Update Build-Depends: pkg-config to pkgconf.

 -- Ryan Tandy <ryan@nardis.ca>  Fri, 08 Mar 2024 18:41:50 -0800

mgba (0.10.2+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062872

 -- Benjamin Drung <bdrung@debian.org>  Wed, 28 Feb 2024 20:00:04 +0000

mgba (0.10.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
    - fixed broken audio in libretro core (Closes: #1036829)

 -- Ryan Tandy <ryan@nardis.ca>  Sat, 17 Jun 2023 11:32:25 -0700

mgba (0.10.1+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Ryan Tandy <ryan@nardis.ca>  Sun, 15 Jan 2023 10:33:17 -0800

mgba (0.10.0+dfsg-2) unstable; urgency=medium

  * Drop libepoxy-dev [armel armhf] from Build-Depends. Fixes FTBFS on those
    arches.

 -- Ryan Tandy <ryan@nardis.ca>  Sun, 01 Jan 2023 23:25:20 +0000

mgba (0.10.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright.
  * Drop patch ffmpeg-5.0, applied upstream.
  * Rename libmgba0.9 to libmgba0.10.
  * mgba-qt.desktop renamed to io.mgba.mGBA.desktop upstream.
  * Update Standards-Version to 4.6.1. No changes needed.
  * Enable the new Lua scripting feature.
    - Add Build-Depends: liblua-5.4-dev.
    - Install example scripts into /usr/share/doc/mgba-common/examples.

 -- Ryan Tandy <ryan@nardis.ca>  Fri, 30 Dec 2022 19:02:08 +0000

mgba (0.9.3+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/copyright: Update upstream contact information.
  * Fix FTBFS with FFmpeg 5.0. (Closes: #1004768)

 -- Ryan Tandy <ryan@nardis.ca>  Wed, 16 Feb 2022 13:49:27 +0100

mgba (0.9.2+dfsg-2) unstable; urgency=medium

  * debian/rules: Link mgba-qt with -latomic. Fixes FTBFS on arches that need
    this support library.

 -- Ryan Tandy <ryan@nardis.ca>  Wed, 10 Nov 2021 19:56:59 -0800

mgba (0.9.2+dfsg-1) experimental; urgency=medium

  [ Ryan Tandy ]
  * New upstream release.
    - Fixed hang on opening a new multiplayer window (Closes: #984695)
  * debian/copyright:
    - Update copyright years.
    - Replace MPL-2.0 license text with a reference to
      /usr/share/common-licenses/MPL-2.0.
  * debian/control: Declare Standards-Version: 4.6.0. No changes required.
  * debian/patches/exclude-*: Add "Forwarded: not-needed" header.
  * Provide a libmgba-dev development package (Closes: #982147):
    - Rename libmgba to libmgba0.9 in accordance with Debian policy for shared
      libraries. Fixes a Lintian warning (package-name-doesnt-match-sonames).
    - Move the libmgba.so development symlink to a new libmgba-dev package.
      Fixes a Lintian warning (link-to-shared-library-in-wrong-package).
    - Install the header files in libmgba-dev.

  [ Markus Koschany ]
  * Change the license of the appdata/metainfo files to CC0-1.0.
    These files are not copyrightable because they do not constitute a creative
    work. The CC0-1.0 license is therefore a good license for meta information.
    (Closes: #919835)

 -- Ryan Tandy <ryan@nardis.ca>  Fri, 03 Sep 2021 10:35:19 -0700

mgba (0.8.4+dfsg-2) unstable; urgency=medium

  * Add upstream patches to remove unused OpenGL code from the libretro core
    and fix its undefined references. (Closes: #986986)
    - debian/patches/CMake-Move-gl.c-and-gles2.c-to-FEATURE_SRC.patch
    - debian/patches/CMake-Move-BUILD_GL-flags-to-FEATURE_DEFINES.patch
  * Add upstream patch to fix PC alignment check when loading a save state.
    - debian/patches/GBA-Serialize-Fix-alignment-check-when-loading-state.patch
  * Add upstream patch to fix crash when unloading a Game Boy ROM in a
    libretro frontend that doesn't implement the camera interface.
    - debian/patches/Libretro-Only-set-camera-peripheral-when-it-is-avail.patch
  * Add upstream patches to fix crashes identified by fuzz testing.
    - debian/patches/Core-Fix-destroying-an-mVL-with-an-invalid-channel-c.patch
    - debian/patches/GB-MBC-Remove-unused-SRAM-size.patch
    - debian/patches/GB-MBC-Force-minimum-SRAM-size-on-rare-MBCs-that-alw.patch
    - debian/patches/GB-Video-Don-t-rendering-negative-batches.patch
    - debian/patches/GB-Video-Fix-deserializing-negative-LX-state.patch
    - debian/patches/GB-Video-Discard-SGB-packets-in-non-SGB-mVLs.patch

 -- Ryan Tandy <ryan@nardis.ca>  Tue, 20 Apr 2021 18:31:14 -0700

mgba (0.8.4+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Add Build-Depends: zipcmp, zipmerge, ziptool as a workaround for bug
    #973280. Fixes FTBFS with libzip 1.7. (Closes: #973069)

 -- Ryan Tandy <ryan@nardis.ca>  Sun, 01 Nov 2020 18:31:10 -0800

mgba (0.8.3+dfsg-2) unstable; urgency=medium

  * Change Build-Depends: libavresample-dev to libswresample-dev.
    (Closes: #971326)

 -- Ryan Tandy <ryan@nardis.ca>  Mon, 28 Sep 2020 18:35:55 -0700

mgba (0.8.3+dfsg-1) unstable; urgency=medium

  * New upstream release.

 -- Ryan Tandy <ryan@nardis.ca>  Thu, 06 Aug 2020 14:57:21 -0700

mgba (0.8.2+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright.

 -- Ryan Tandy <ryan@nardis.ca>  Wed, 15 Jul 2020 01:25:08 +0200

mgba (0.8.1+dfsg-1) unstable; urgency=medium

  * New upstream release. (Closes: #950820)
  * Exclude vendored rapidjson; add Build-Depends: rapidjson-dev.
    - CMakeLists.txt: don't install excluded res/licenses/rapidjson.txt.
  * Exclude vendored inih; add Build-Depends: libinih-dev. (Closes: #958247)
    - CMakeLists.txt: don't install excluded res/licenses/inih.txt.
  * Update debian/copyright with newly added third-party licenses.
  * Update Build-Depends: cmake >= 3.1 (since 0.7.0).
  * Add Build-Depends: libavfilter-dev to re-enable GIF/Video recording
    feature.
  * Update Standards-Version to 4.5.0. No changes needed.
  * Drop -Wl,--as-needed as this is now the default.
  * Update to debhelper compat level 13.
    - libretro-mgba.install: Use built-in substitution instead of dh-exec.
    - Update *.install and *.manpages to install from debian/tmp.
    - Add debian/not-installed.
  * Drop patch 01_fix-about-strings. The patch was out of date and the git
    revision info is not required since we build from a release tarball.
  * Add debian/gitlab-ci.yaml for CI build.
  * Add myself to Uploaders.

 -- Ryan Tandy <ryan@nardis.ca>  Fri, 29 May 2020 02:04:36 +0200

mgba (0.7.0-1) unstable; urgency=medium

  * Team upload.

  [ Jeremy Bicha ]
  * Import packaging to https://salsa.debian.org/ (Closes: #872484)
  * Add .libretro file for integration with GNOME Games. (Closes: #911327)
  * Add minimal debian/gbp.conf.
  * Use dh-exec instead of manual .sh script for install file.

  [ Reiner Herrmann ]
  * New upstream release.
  * Drop patch applied upstream and refresh other one.
  * Update Standards-Version to 4.3.0.
    - Declare that debian/rules does not require root.
  * Depend on debhelper-compat and bump compat level to 12.
  * Add upstream metadata.
  * Add libelf-dev as new build dependency.
  * Update debian/copyright.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 03 Feb 2019 23:53:12 +0100

mgba (0.6.3+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * debian/patches/7f41dd354176b720c8e3310553c6b772278b9dca.patch:
     - upstream build fix with new qt 5.11

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Thu, 02 Aug 2018 16:43:18 +0200

mgba (0.6.3+dfsg1-1) unstable; urgency=low

  * New upstream release (Closes: #876451).
  * Removed patches (fixed on upstream).
  * Added missing libsqlite3-dev and qttools5-dev-tools dependencies.
  * Added AppStream desktop & addon meta-info files.

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Mon, 04 Jun 2018 23:00:00 -0300

mgba (0.5.2+dfsg1-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add upstream fix for FTBFS with Qt 5.10,
    thanks to Juhani Numminen. (Closes: #895496)
  * Fix FTBFS on armel. (Closes: #884392)
  * Add patch from James Cowgill to fix FTBFS with FFmpeg 4.0.
    (Closes: #888325)

 -- Adrian Bunk <bunk@debian.org>  Thu, 10 May 2018 17:53:34 +0300

mgba (0.5.2+dfsg1-3) unstable; urgency=medium

  * Team upload.
  * Add gcc7.patch and fix FTBFS with GCC 7. (Closes: #853546)
    Thanks to Adrian Bunk for the patch.
  * Declare compliance with Debian Policy 4.0.1.
  * Switch to compat level 10.
  * Use https for Format field.

 -- Markus Koschany <apo@debian.org>  Thu, 17 Aug 2017 21:39:12 +0200

mgba (0.5.2+dfsg1-2) unstable; urgency=low

  * Fix GB/GBC saves in the Libretro port.

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Wed, 04 Jan 2017 23:38:00 -0300

mgba (0.5.2+dfsg1-1) unstable; urgency=low

  * New upstream release.

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Sat, 31 Dec 2016 18:35:00 -0300

mgba (0.4.1+dfsg1-1) unstable; urgency=low

  * New upstream release.
  * Changed package maintainer to Debian Games Team.
  * Added a common package for shaders and DAT.
  * Fixed hardening-no-bindnow and hardening-no-pie.
  * Bump Standards-Version to 3.9.8.

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Sun, 17 Jul 2016 23:04:00 -0300

mgba (0.4.0+dfsg1-3) unstable; urgency=low

  * Removed debug packages from control (automatic -dbgsym packages).

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Sun, 10 Apr 2016 22:12:00 -0300

mgba (0.4.0+dfsg1-2) unstable; urgency=low

  * Simplify debug packages long description.
  * Add comments to Copyright and use standard Zlib license name.

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Thu, 07 Apr 2016 17:50:00 -0300

mgba (0.4.0+dfsg1-1) unstable; urgency=low

  * Initial release (Closes: #787470).

 -- Sérgio Benjamim <sergio_br2@yahoo.com.br>  Wed, 06 Apr 2016 00:02:00 -0300
